package com.example.ffbf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class RestaurantActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant);
    }

    public void AddRestaurantFood(View view) {
        startActivity(new Intent(RestaurantActivity.this, AddRestaurantActivity.class));
        finish();
    }

    public void ListRestaurantFood(View view) {
        startActivity(new Intent(RestaurantActivity.this, ListRestaurantActivity.class));
        finish();
    }


}