package com.example.ffbf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;

public class MainActivity extends AppCompatActivity {

    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        auth = FirebaseAuth.getInstance();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

//    @Override
//    protected void onStart() {
//        super.onStart();
//        if (auth.getCurrentUser() == null) {
//            startActivity(new Intent(MainActivity.this, Login.class));
//            finish();
//        }
//    }

    @Override
    protected void onStart() {
        super.onStart();
        if (auth.getCurrentUser() == null) {
            startActivity(new Intent(MainActivity.this, Login.class));
            finish();
        }
    }

    public void Restaurant(View view) {
        startActivity(new Intent(MainActivity.this, RestaurantActivity.class));
        finish();
    }

    public void StreetFood(View view) {
        startActivity(new Intent(MainActivity.this, StreetFoodActivity.class));
        finish();
    }

    public void MyProfile(View view) {
        startActivity(new Intent(MainActivity.this, ProfileActivity.class));
        finish();
    }

    public void logOut(View view) {
        auth.signOut();
        Toast.makeText(this, "Logout Successful", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(MainActivity.this, Login.class));
        finish();
    }

}