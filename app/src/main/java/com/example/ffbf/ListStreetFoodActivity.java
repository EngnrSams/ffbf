package com.example.ffbf;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class ListStreetFoodActivity extends AppCompatActivity implements RecyclerAdapter.OnItemClickListener {

    private RecyclerView mRecyclerView;
    private RecyclerAdapter mAdapter;
    private ProgressBar mProgressBar;
    private FirebaseStorage mStorage;
    private DatabaseReference mDatabaseRef;
    private ValueEventListener mDBListener;
    private List<Restaurant> mRestaurants;

    private void openDetailActivity(String[] data){
        Intent intent = new Intent(this, StreetFoodDetailsActivity.class);
        intent.putExtra("NAME_KEY",data[0]);
        intent.putExtra("DESCRIPTION_KEY",data[1]);
        intent.putExtra("IMAGE_KEY",data[2]);
        startActivity(intent);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_list_street_food );

        mRecyclerView = findViewById(R.id.mRecyclerView);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));

        mProgressBar = findViewById(R.id.myDataLoaderProgressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        mRestaurants = new ArrayList<>();
        mAdapter = new RecyclerAdapter (ListStreetFoodActivity.this, mRestaurants);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.setOnItemClickListener(ListStreetFoodActivity.this);

        mStorage = FirebaseStorage.getInstance();
        mDatabaseRef = FirebaseDatabase.getInstance().getReference("street_foods");

        mDBListener = mDatabaseRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                mRestaurants.clear();

                for (DataSnapshot restaurantSnapshot : dataSnapshot.getChildren()) {
                    Restaurant uploadId = restaurantSnapshot.getValue(Restaurant.class);
                    uploadId.setKey(restaurantSnapshot.getKey());
                    mRestaurants.add(uploadId);
                }
                mAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Toast.makeText(ListStreetFoodActivity.this, databaseError.getMessage(), Toast.LENGTH_SHORT).show();
                mProgressBar.setVisibility(View.INVISIBLE);
            }
        });

    }
    public void onItemClick(int position) {
        Restaurant clickedRestaurant=mRestaurants.get(position);
        String[] restaurantData={clickedRestaurant.getRestaurantName(),clickedRestaurant.getRestaurantDesc(),clickedRestaurant.getImageURL()};
        openDetailActivity(restaurantData);
    }

    @Override
    public void onShowItemClick(int position) {
        Restaurant clickedRestaurant=mRestaurants.get(position);
        String[] restaurantData={clickedRestaurant.getRestaurantName(),clickedRestaurant.getRestaurantDesc(),clickedRestaurant.getImageURL()};
        openDetailActivity(restaurantData);
    }

    @Override
    public void onDeleteItemClick(int position) {
        Restaurant selectedItem = mRestaurants.get(position);
        final String selectedKey = selectedItem.getKey();

        StorageReference imageRef = mStorage.getReferenceFromUrl(selectedItem.getImageURL());
        imageRef.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mDatabaseRef.child(selectedKey).removeValue();
                Toast.makeText(ListStreetFoodActivity.this, "Item deleted", Toast.LENGTH_SHORT).show();
            }
        });

    }
    protected void onDestroy() {
        super.onDestroy();
        mDatabaseRef.removeEventListener(mDBListener);
    }

}