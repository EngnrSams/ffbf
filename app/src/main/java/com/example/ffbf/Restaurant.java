package com.example.ffbf;


import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class Restaurant {
    private String restaurantName;
    private String restaurantDesc;
    private String imageURL;
    private String key;


    public Restaurant(){

    }

    public Restaurant(String restaurantName, String restaurantDesc, String imageURL) {
        this.restaurantName = restaurantName;
        this.restaurantDesc = restaurantDesc;
        this.imageURL = imageURL;
    }
    public String getRestaurantName() {
        return restaurantName;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }

    public String getRestaurantDesc() {
        return restaurantDesc;
    }

    public void setRestaurantDesc(String restaurantDesc) {
        this.restaurantDesc = restaurantDesc;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Exclude
    public String getKey() {
        return key;
    }
    @Exclude
    public void setKey(String key) {
        this.key = key;
    }
}
