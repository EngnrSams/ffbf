package com.example.ffbf;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Timer;
import java.util.TimerTask;

public class Register extends AppCompatActivity {

    EditText inputFname,inputSname,inputEmail,inputUsername,inputPassword;
    Button registerBtn;
    TextView loginButton;
    FirebaseAuth auth;
    ProgressBar progressBar;
    private DatabaseReference mDatabaseRef;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        auth = FirebaseAuth.getInstance();
        inputUsername = (EditText) findViewById(R.id.username);
        inputFname = (EditText) findViewById(R.id.fname);
        inputSname = (EditText) findViewById(R.id.sname);
        inputEmail = (EditText) findViewById(R.id.email);
        inputPassword = (EditText) findViewById(R.id.password);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
    }

    public void loginPage(View view) {
        startActivity(new Intent(Register.this, Login.class));
        finish();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(Register.this, MainActivity.class));
            finish();
        }
    }

    public void Register(View view) {

        final String username = inputUsername.getText().toString().trim();
        final String email = inputEmail.getText().toString().trim();
        final String fname = inputFname.getText().toString().trim();
        final String sname = inputSname.getText().toString().trim();

        String password = inputPassword.getText().toString().trim();

        if (TextUtils.isEmpty(username)) {
            inputUsername.setError("Enter your name!");
            inputUsername.requestFocus();
            return;
        }
        if (TextUtils.isEmpty(email)) {
            inputEmail.setError("Enter email address!");
            inputEmail.requestFocus();
            return;
        }
        if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            inputEmail.setError("Email not Valid...!");
            inputEmail.requestFocus();
            return;
        }

        if (TextUtils.isEmpty(password)) {
            inputPassword.setError("Enter password!");
            inputPassword.requestFocus();
            return;
        }
        if (password.length() < 6) {
            inputPassword.setError("Password too short, enter minimum 6 characters!");
            inputPassword.requestFocus();
            return;
        }

        progressBar.setVisibility(View.VISIBLE);


        auth.createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            Toast.makeText(Register.this, "User Created Successfully", Toast.LENGTH_SHORT).show();
                            FirebaseUser user = auth.getCurrentUser();
                            String userID = user.getUid();
//                            String email = user.getEmail();

                            mDatabaseRef = FirebaseDatabase.getInstance().getReference("users");
//                            Restaurant upload = new Restaurant(name.getText().toString().trim(), rest_desc.getText().toString(), downloadUri.toString());
                            UserProfileData newUserProData = new UserProfileData(fname, sname, username, email);
                            String uploadId = mDatabaseRef.push().getKey();
                            mDatabaseRef.child(userID).setValue(newUserProData);


                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    progressBar.setVisibility(View.GONE);
                                    startActivity(new Intent(Register.this, MainActivity.class));
                                    finish();
                                }
                            }, 5 * 1000);

                        } else {
                            // If sign in fails, display a message to the user.
//                            Log.w(TAG, "createUserWithEmail:failure", task.getException());
                            Toast.makeText(Register.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
//                            updateUI(null);/
                        }

                        // ...
                    }
                });


    }

}