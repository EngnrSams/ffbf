package com.example.ffbf;

import com.google.firebase.database.Exclude;

public class StreetFood {
    private String streetfoodName;
    private String streetfoodDesc;
    private String imageURL;
    private String key;

    public StreetFood(){

    }

    public StreetFood(String restaurantName, String restaurantDesc, String imageURL) {
        this.streetfoodName = restaurantName;
        this.streetfoodDesc = restaurantDesc;
        this.imageURL = imageURL;
    }
    public String getRestaurantName() {
        return streetfoodName;
    }

    public void setRestaurantName(String restaurantName) {
        this.streetfoodName = restaurantName;
    }

    public String getRestaurantDesc() {
        return streetfoodDesc;
    }

    public void setRestaurantDesc(String restaurantDesc) {
        this.streetfoodDesc = restaurantDesc;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    @Exclude
    public String getKey() {
        return key;
    }
    @Exclude
    public void setKey(String key) {
        this.key = key;
    }

}
