package com.example.ffbf;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class StreetFoodActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_street_food);
    }

    public void AddStreetFood(View view) {
        startActivity(new Intent(StreetFoodActivity.this, AddStreetFoodActivity.class));
        finish();
    }

    public void ListStreetFood(View view) {
        startActivity(new Intent(StreetFoodActivity.this, ListStreetFoodActivity.class));
        finish();
    }

}